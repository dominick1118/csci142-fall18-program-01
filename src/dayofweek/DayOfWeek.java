package dayofweek;
/**
 * 
 * @author Dom
 * @version 1.0
 *
 */

public class DayOfWeek 
{
	// creating all the necessary variables here. pretty simple stuff.
	public final static int NO_VALUE = -1;
	public int myMonth = NO_VALUE;
	public int myDay = NO_VALUE;
	public int myYear = NO_VALUE;
	private String[] myMonthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	private String[] myWeekdays = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	private int[] myDaysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	private int[] myDaysInLeapMonth = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	public boolean leapYear = false;
	public int myDayOfWeek = NO_VALUE;
	private int[] myAdjustments = {1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6};
	public int monthAdjustment = NO_VALUE;
	public int myYearRemainder = NO_VALUE;
	public int myDayOfWeekSum = NO_VALUE;
	public boolean invalidDate = false;
	
	// basic stuff here. you tell the program what month, day of month, and year you want to input, it checks to make sure your input is valid, and puts it in.
	public DayOfWeek(int month, int dayOfMonth, int year) 
	{
		int myYearRemainder = year%4;
		boolean checkMonth = (month >= 1) && (month <=12);
		boolean checkDay = (dayOfMonth >= 1 && dayOfMonth <= myDaysInLeapMonth[month-1]);
		boolean checkYear = (myYearRemainder == 0) && (year >= 1900 && year <=2099);
		if (checkMonth && checkDay && checkYear) 
		{
			myMonth = month;
			myDay = dayOfMonth;
			myYear = year;
			leapYear = true;
			calculateDayOfWeek();
		}
		else if ((month >=1 && month <=12) && (myYearRemainder != 0) && (dayOfMonth >=1 && dayOfMonth <= myDaysInMonth[month-1]) && (year >= 1900 && year <= 2099)) 
		{
			myMonth = month;
			myDay = dayOfMonth;
			myYear = year;
			leapYear = false;
			calculateDayOfWeek();
		} 
		else 
		{
			invalidDate = true;
			myMonth = NO_VALUE;
			myDay = NO_VALUE;
			myYear = NO_VALUE;
			leapYear = false;
			myDayOfWeek = NO_VALUE;
		}
	}
	
	// calculates the day of week based on the date you gave.
	private int calculateDayOfWeek() 
	{
		myYear = myYear - 1900;
		int yearAdjustment = (int) Math.floor(myYear/4);
		if(((myMonth == 1) || (myMonth == 2)) && leapYear == true) 
		{ 
			monthAdjustment = (myAdjustments[myMonth -1] - 1);
		}
		else 
		{
			monthAdjustment = myAdjustments[myMonth - 1];
		}
		myYearRemainder = (int) Math.rint(myYearRemainder);
		myDayOfWeekSum = monthAdjustment + myDay + myYear + yearAdjustment;
		myDayOfWeek = myDayOfWeekSum%7;
		return myDayOfWeek;
	}
	
	// spits out the numeric day of week based on the date you gave.
	public int getNumericDayOfWeek() 
	{
		return myDayOfWeek;
	}
	
	// spits out the day of week based on the date you gave. pretty much the whole point of the program.
	public String getDayOfWeek() 
	{
		if(invalidDate == false) 
	    {
			return myWeekdays[myDayOfWeek - 1];
		}
		else
		{
			return null;
		}
	}
	
	// spits out the month number you gave. personally i don't see why you can't just remember this, but whatever i guess.
	public int getMonth() 
	{
		return myMonth;
	}
	
	// in case you can't figure out for yourself, this program is also handy for telling you the name of the month you gave.
	public String getMonthString() 
	{
		if(invalidDate == false)
		{
			return myMonthNames[myMonth - 1];
		}
		else
		{
			return null;
		}
	}
	
	// the program can also tell you the day you put in! pretty handy i guess.
	public int getDayOfMonth() 
	{
		return myDay;
	}
	
	// in case of amnesia, you can also ask the program what year you input, and it spits it back at you. pretty sweet, huh?
	public int getYear() 
	{
		return myYear;
	}

}
